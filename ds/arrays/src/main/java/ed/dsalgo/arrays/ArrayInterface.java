package ed.dsalgo.arrays;

public interface ArrayInterface {
	void traverse();

	// insert
	void insert(Object pos, int i);

	// read
	int read(Object update);

	// pos
	void update(Object pos, int val);

	// delete
	void delete(Object pos);

	// sort
	void sort();

	// search
	Object search(int val);
}
