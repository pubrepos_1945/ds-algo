package ed.dsalgo.arrays;

import ed.dsalgo.arrays.*;
import ed.dsalgo.arrays.utils.Position;


public class Demo {
	public static void main(String[] args) {
	    
	    DynamicArray2DDemo();

	}
        private static void DynamicArray2DDemo(){
	    DynamicArray2D 	    da2 = new  DynamicArray2D (2,2);
		try {
			System.out.println("Inserting 3 elements");

			da2.insert(new Position(0, 0), 1);
			da2.insert(new Position(0,1), 2);
			da2.insert(new Position(1,0), 3);
			da2.insert(new Position(1,1), 4);			
			da2.traverse();

			//read
			System.out.println("reading 2nd element: " + da2.read(new Position(0,1)));
			
			//update
			System.out.println("updating 2nd element to 20: ");
			da2.update(new Position(0,1),20);
			da2.traverse();
			

			//delete
			System.out.println("deleting 2nd element to 20: ");
			da2.delete(new Position(0,1));
			da2.traverse();

			//search
			System.out.println("searching for 4: ");
			System.out.println(da2.search(4));

			da2.traverse();

			

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

    }
    private static void DynamicArrayDemo(){
			DynamicArray da = new DynamicArray(10);
		try {
			System.out.println("Inserting 3 elements");
			da.insert(0, 1);
			da.insert(1, 2);
			da.insert(2, 3);
			da.traverse();
			System.out.println("Traversing 3  elements");
			da.traverse(0, 2);
			System.out.println("Inserting 1 element that is out of bound");
			da.insert(10, 11);
			da.traverse();
			System.out.println("Reading 11th  element");
			System.out.println(da.read(10));
			System.out.println("Reading 11th  element: 11");
			System.out.println(da.search(10));
			System.out.println("Reading element: 7");
			System.out.println(da.search(7));
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

    }
}
