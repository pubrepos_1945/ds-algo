package ed.dsalgo.arrays;

import java.util.NoSuchElementException;
import ed.dsalgo.arrays.utils.Position;

public class DynamicArray2D implements ArrayInterface {
	private int x_len;
	private int y_len;
	private static final int OFFSET_X = 2;
	private static final int OFFSET_Y = 2;
	private int[][] arr;

	public DynamicArray2D(int h, int b) {
		this.x_len = h;
		this.y_len = b;
		arr = new int[h][b];
	}

	private Position unWrapPos(Object pos) {
		return ((Position) pos);
	}

	// traverse
	public void traverse() {
		System.out.println("\nPrinting all the elements of the 2d Array \n");
		System.out.print("\n");
		for (int j = 0; j < arr.length; j++) {
			for (int k = 0; k < arr[j].length; k++)
				System.out.print(" " + arr[j][k] + " ");
			System.out.println();
		}

	}

	// insert
	public void insert(Object pos, int i) {
		Position _pos = unWrapPos(pos);
		if (_pos.x < 0 || _pos.y < 0) {
			throw new IllegalArgumentException(
					"insert position in the array should be greater than or equal to 0");
		}

		if (_pos.x > this.x_len - 1 || _pos.y > this.y_len - 1) {
			System.out.println("\n");
			System.out.println("Array capacity reached. Increasing capacity by default inc_offset");
			System.out.println("\n");
			this.x_len = this.x_len + OFFSET_X;
			this.y_len = this.y_len + OFFSET_Y;
			int[][] _arr = new int[x_len][y_len];

			for (int j = 0; j < arr.length; j++) {
				for (int k = 0; k < arr[j].length; k++)
					_arr[j][k] = arr[j][k];
			}
			this.arr = _arr;
		}

		this.arr[_pos.x][_pos.y] = i;
	}

	// read
	public int read(Object pos) {
		Position _pos = unWrapPos(pos);
		System.out.println("\n");
		if (_pos.x < 0 || _pos.y < 0) {
			throw new IllegalArgumentException(
					"insert position in the array should be greater than or equal to 0");
		}
		if (_pos.x > this.x_len - 1 || _pos.y > this.y_len - 1) {
			throw new IllegalArgumentException(
					"insert position in the array should be less than the length of the array");
		}
		return this.arr[_pos.x][_pos.y];
	}

	// update
	public void update(Object pos, int val) {
		Position _pos = unWrapPos(pos);
		System.out.println("\n");

		if (_pos.x < 0 || _pos.y < 0) {
			throw new IllegalArgumentException(
					"update position in the array should be greater than or equal to 0");
		}
		if (_pos.x > this.x_len - 1 || _pos.y > this.y_len - 1) {
			throw new IllegalArgumentException(
					"update position in the array should be less than the length of the array");
		}
		this.arr[_pos.x][_pos.y] = val;
	}

	// delete
	public void delete(Object pos) {
		Position _pos = unWrapPos(pos);
		System.out.println("\n");
		if (_pos.x < 0 || _pos.y < 0) {
			throw new IllegalArgumentException(
					"delete position in the array should be greater than or equal to 0");
		}
		if (_pos.x > this.x_len - 1 || _pos.y > this.y_len - 1) {
			throw new IllegalArgumentException(
					"delete position in the array should be less than the length of the array ");
		}

		this.arr[_pos.x][_pos.y] = 0;
	}

	// sort
	public void sort() {

	}

	// search
	public Position search(int val) {
		for (int j = 0; j < arr.length; j++) {
			for (int k = 0; k < arr[j].length; k++)
				if (this.arr[j][k] == val)
				    return new Position(j,k);
		}

		throw new NoSuchElementException(val + " not found in the array");

	}

}

