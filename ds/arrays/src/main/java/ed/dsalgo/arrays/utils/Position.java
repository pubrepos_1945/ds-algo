package ed.dsalgo.arrays.utils;

public class Position {
	public Position(int x, int y) {
		this.x = x;
		this.y = y;
	}

	@Override
	public String toString() {
		return "x: " + this.x + " y: " + this.y;
	}

	public int x;
	public int y;
}
